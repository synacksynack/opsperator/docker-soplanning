<?php
$cfgHostname = 'DB_HOST';
$cfgDatabase = 'DB_NAME';
$cfgUsername = 'DB_USER';
$cfgPassword = 'DB_PASS';
$cfgSqlType = 'mysql';
$cfgPrefix = 'DB_PREFIX';
