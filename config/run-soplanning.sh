#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=true
else
    DO_DEBUG=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
OPENLDAP_BIND_DN_PREFIX=${OPENLDAP_BIND_DN_PREFIX:-cn=soplanning,ou=services}
OPENLDAP_BIND_PW=${OPENLDAP_BIND_PW:-secret}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
SOPLANNING_ADMIN_PASSWORD=${SOPLANNING_ADMIN_PASSWORD:-secret}
SOPLANNING_DB_NAME=${SOPLANNING_DB_NAME:-soplanning}
SOPLANNING_DB_HOST=${SOPLANNING_DB_HOST:-soplanning-mysql}
SOPLANNING_DB_PASSWORD="${SOPLANNING_DB_PASSWORD:-secret}"
SOPLANNING_DB_USER="${SOPLANNING_DB_USER:-soplanning}"
SMTP_HOST=${SMTP_HOST:-smtp.demo.local}
SMTP_PORT=${SMTP_PORT:-25}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=planning.$OPENLDAP_DOMAIN
fi
if test "$OPENLDAP_HOST"; then
    DO_LDAP=true
else
    DO_LDAP=false
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$OPENLDAP_USER_OBJECTCLASS"; then
    OPENLDAP_USER_OBJECTCLASS="inetOrgPerson"
fi
if test -z "$SOPLANNING_SITE_NAME"; then
    SOPLANNING_SITE_NAME=KubePlanning
fi

install_site()
{
    if test "`id -u`" = 0; then
	rsync_options="-rlDog --chown www-data:root"
    else
	rsync_options=-rlD
    fi
    rsync $rsync_options --exclude config.inc --exclude database.inc \
	--delete /usr/src/soplanning/ /var/www/html/
}

hash_pw()
{
    php -r "echo sha1(mb_convert_encoding(\"¤\" . \"$1\" . \"¤\", \"ISO-8859-1\", \"UTF-8\"));"
}

if ! test -s /var/www/html/index.php; then
    echo INFO: initializing site
    install_site
fi
if ! test -s /var/www/html/database.inc; then
    if test -s /var/www/html/sql/planning_mysql.sql; then
	echo INFO: initializing database
	if test -z "$TZ"; then
	    TZ=UTC
	fi
	DEFAULT_ADMIN=df5b909019c9b1659e86e0d6bf8da81d6fa3499e
	ADMIN_HASH=$(hash_pw "$SOPLANNING_ADMIN_PASSWORD")
	if test -z "$SMTP_MAIL_FROM"; then
	    SMTP_MAIL_FROM=soplanning@$OPENLDAP_DOMAIN
	fi
	sed -e "s|$DEFAULT_ADMIN|$ADMIN_HASH|" \
	    -e "s|SMTP_HOST', 'localhost'|SMTP_HOST', '$SMTP_HOST'|" \
	    -e "s|SMTP_PORT', ''|SMTP_PORT', '$SMTP_PORT'|" \
	    -e "s|SMTP_FROM', ''|SMTP_FROM', '$SMTP_MAIL_FROM'|" \
	    -e "s|SOPLANNING_TITLE', 'SOPLanning'|SOPLANNING_TITLE', '$SOPLANNING_SITE_NAME'|" \
	    -e "s|TIMEZONE', 'Europe/Paris'|TIMEZONE', '$TZ'|" \
	    /var/www/html/sql/planning_mysql.sql \
	    | mysql \
		-u "$SOPLANNING_DB_USER" \
		"--password=$SOPLANNING_DB_PASSWORD" \
		-h "$SOPLANNING_DB_HOST" \
		"$SOPLANNING_DB_NAME"
	(
	    cat <<EOF
DELETE FROM planning_user WHERE user_id = 'user1';
DELETE FROM planning_user WHERE user_id = 'user2';
DELETE FROM planning_user WHERE user_id = 'user3';
DELETE FROM planning_user WHERE user_id = 'publicspl';
UPDATE planning_user SET visible_planning = 'non' WHERE user_id = 'admin';
EOF
	) | mysql \
	    -u "$SOPLANNING_DB_USER" \
	    "--password=$SOPLANNING_DB_PASSWORD" \
	    -h "$SOPLANNING_DB_HOST" \
		"$SOPLANNING_DB_NAME"
    fi
    echo INFO: install database configuration
    sed -e "s|DB_HOST|$SOPLANNING_DB_HOST|" \
	-e "s|DB_NAME|$SOPLANNING_DB_NAME|" \
	-e "s|DB_USER|$SOPLANNING_DB_USER|" \
	-e "s|DB_PASS|$SOPLANNING_DB_PASSWORD|" \
	-e "s|DB_PREFIX|planning_|" \
	/database.inc >/var/www/html/database.inc
fi

if ! test -s /var/www/html/config.inc; then
    echo INFO: Configure Site
    if test "$OPENLDAP_PROTO" = ldaps; then
	LDAP_USE_TLS=false
    else
	LDAP_USE_TLS=${OPENLDAP_STARTTLS:-false}
    fi
    if ! test "$LDAP_AUTO_CREATE" = false; then
	LDAP_AUTO_CREATE=true
    fi
    if test "$OPENLDAP_HOST"; then
	LDAP_URL="$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT"
    else
	LDAP_URL=
    fi
    sed -e "s|LDAP_URL|$LDAP_URL|" \
	-e "s|LDAP_DOMAIN|$OPENLDAP_DOMAIN|" \
	-e "s|LDAP_BASE|$OPENLDAP_BASE|" \
	-e "s|LDAP_USER_OC|$OPENLDAP_USER_OBJECTCLASS|" \
	-e "s|LDAP_USE_TLS|$LDAP_USE_TLS|" \
	-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
	-e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	-e "s|DO_LDAP|$DO_LDAP|" \
	-e "s|LDAP_DO_CREATE|$LDAP_AUTO_CREATE|" \
	-e "s|DO_DEBUG|$DO_DEBUG|" \
	/config.inc >/var/www/html/config.inc
else
    left=$(cat version.txt)
    right=$(cat /usr/src/soplanning/version.txt)
    if ! test "$left" = "$right"; then
	eval `echo "$left" | sed 's|\([0-9]*\)\.\([0-9]*\)[\.]\([0-9]*\)|l1=\1 l2=\2 l3=\3|'`
	ls /usr/src/soplanning/sql/update/*.txt | sort -n | while read patch
	    do
		eval `echo $patch | sed 's|.*update-\([0-9]*\)-\([0-9]*\)[-]*\([0-9]*\)\.txt|r1=\1 r2=\2 r3=\3 upd=false|'`
		if test "$l1" -lt "$r1"; then
		    upd=true
		elif test "$l1" -eq "$r1"; then
		    if test "$l2" -lt "$r2"; then
			upd=true
		    elif test "$l2" -eq "$r2"; then
			if test "$l3" -a "$r3" -a "$l3" -lt "$r3"; then
			    upd=true
			elif test "$r3" -a -z "$l3"; then
			    upd=true
			fi
		    fi
		fi
		if $upd; then
		    echo "INFO: applying $patch"
		    if ! cat "$patch" | mysql \
			    -u "$SOPLANNING_DB_USER" \
			    "--password=$SOPLANNING_DB_PASSWORD" \
			    -h "$SOPLANNING_DB_HOST" \
			    "$SOPLANNING_DB_NAME"; then
			echo WARNING: failed applying
			break
		    fi
		fi
	    done
	echo INFO: now updating site data
	install_site
    fi
fi

export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=yay
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO=$SOPLANNING_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

echo "Install SOPlanning Site Configuration"
sed -e "s|SOPLANNING_HOSTNAME|$APACHE_DOMAIN|g" \
    -e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
    -e "s|SSL_TOGGLE_INCLUDE|$SSL_INCLUDE.conf|g" \
    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf

unset SSL_INCLUDE LDAP_USE_TLS LDAP_URL OPENLDAP_USER_FILTER SMTP_HOST \
    SOPLANNING_DB_NAME SOPLANNING_ADMIN_PASSWORD SMTP_MAIL_FROM \
    DO_DEBUG SOPLANNING_PROTO ADMIN_HASH DEFAULT_ADMIN \
    SOPLANNING_DB_HOST SOPLANNING_DB_PASSWORD SOPLANNING_DB_USER

. /run-apache.sh
