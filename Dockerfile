FROM opsperator/php

# SOPlanning image for OpenShift Origin

ARG DO_UPGRADE=
ENV DLQS="https%3A%2F%2Fsourceforge.net%2Fprojects%2Fsoplanning%2Ffiles%2Flatest%2Fdownload&ts=42424242" \
    DLURL=https://downloads.sourceforge.net/project/soplanning/soplanning \
    SOVERSION=1.49.00

LABEL io.k8s.description="SOPlanning $SOVERSION" \
      io.k8s.display-name="SOPlanning" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="soplanning" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-soplanning" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$SOVERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && echo precedence ::ffff:0:0/96 100 >>/etc/gai.conf \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install SOPlanning Dependencies" \
    && apt-get -y install --no-install-recommends mariadb-client \
	postgresql-client ldap-utils openssl rsync wget zlib1g libpng16-16 \
	libjpeg62-turbo libwebp6 libgd3 \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install --no-install-recommends libldap2-dev unzip \
	zlib1g-dev libfreetype6-dev libjpeg62-turbo-dev libwebp-dev \
        libpng-dev \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install -j "$(nproc)" gd ldap mysqli pdo_mysql \
    && echo "# Install SOPlanning" \
    && ( \
	for retry in 1 2 3; \
	do \
	    wget "$DLURL/$SOVERSION/soplanning.zip?r=$DLQS" \
		--no-check-certificate -O /usr/src/soplanning.zip && break; \
	    rm -f /usr/src/soplanning.zip; \
	done; \
	if ! test -s /usr/src/soplanning.zip; then \
	    echo Could not fetch SOPlanning - bailing out; \
	    exit 1; \
	fi; \
    ) \
    && ( \
	cd /usr/src/ \
	&& unzip soplanning.zip \
	&& mv soplanning/config.inc soplanning/config.inc.sample \
	&& mv soplanning/database.inc soplanning/database.inc.sample \
	&& rm -f soplanning/README.txt soplanning/INSTALL.txt \
	    soplanning/LICENSE.txt \
    ) \
    && echo "# Patch SOPlanning" \
    && patch -d/ -p0 </login.patch \
    && echo "# Fixing Permissions" \
    && for dir in /usr/src/soplanning /etc/lemonldap-ng; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning Up" \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && a2dismod perl \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz /usr/src/soplanning.zip /login.patch \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-soplanning.sh"]
USER 1001
