# k8s SOPlanning

SOPlanning image. Includes patches auto-provisionning users from LDAP.
And bug, during first login, looks like several headers are sent.

Diverts from https://github.com/faust64/docker-php

Build with:
```
$ make build
```

Test with:
```
$ make demo
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name             |    Description                 | Default                                                     | Inherited From    |
| :--------------------------- | ------------------------------ | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`             | SOPlanning VirtualHost         | `planning.${OPENLDAP_DOMAIN}`                               | opsperator/apache |
|  `APACHE_HTTP_PORT`          | SOPlanning Directory HTTP Port | `8080`                                                      | opsperator/apache |
|  `APACHE_IGNORE_OPENLDAP`    | Ignore LemonLDAP autoconf      | undef                                                       | opsperator/apache |
|  `LDAP_AUTO_CREATE`          | Auto-Create SOPlanning users   | `true`                                                      |                   |
|  `OPENLDAP_BASE`             | OpenLDAP Base                  | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`   | OpenLDAP Bind DN Prefix        | `cn=soplanning,ou=services`                                 | opsperator/apache |
|  `OPENLDAP_BIND_PW`          | OpenLDAP Bind Password         | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`   | OpenLDAP Conf DN Prefix        | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`           | OpenLDAP Domain Name           | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`             | OpenLDAP Backend Address       | undef                                                       | opsperator/apache |
|  `OPENLDAP_PORT`             | OpenLDAP Bind Port             | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`            | OpenLDAP Proto                 | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_USER_OBJECTCLASS` | OpenLDAP User ObjectClass      | `inetOrgPerson`                                             |                   |
|  `OPENLDAP_STARTTLS`         | OpenLDAP Start TLS             | `false`                                                     |                   |
|  `PHP_ERRORS_LOG`            | PHP Errors Logs Output         | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`    | PHP Max Execution Time         | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`      | PHP Max File Uploads           | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`         | PHP Max Post Size              | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`   | PHP Max Upload File Size       | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`          | PHP Memory Limit               | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`              | SOPlanning Public Proto        | `http`                                                      |                   |
|  `SOPLANNING_ADMIN_PASSWORD` | SOPlanning Admin Password      | `secret`                                                    |                   |
|  `SOPLANNING_DB_NAME`        | SOPlanning MySQL Database      | `soplanning`                                                |                   |
|  `SOPLANNING_DB_HOST`        | SOPlanning MySQL Hostname      | `soplanning-mysql`                                          |                   |
|  `SOPLANNING_DB_PASSWORD`    | SOPlanning MySQL Password      | `secret`                                                    |                   |
|  `SOPLANNING_DB_USER`        | SOPlanning MySQL Username      | `soplanning`                                                |                   |
|  `SOPLANNING_SITE_NAME`      | SOPlanning Site Name           | `KubePlanning`                                              |                   |
|  `SMTP_HOST`                 | SOPlanning SMTP Host           | undef                                                       |                   |
|  `SMTP_PORT`                 | SOPlanning SMTP Port           | `25`                                                        |                   |

You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
|  `/var/www/html     | SOPlanning site path            |                   |
